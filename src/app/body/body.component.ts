import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  externalLink: string;
  constructor() { }

  ngOnInit() {
    this.externalLink = 'http://www.ace-net.ca/wp-content/uploads/2015/05/ACENET-Annual-Report-2014-2015.pdf';
  }

}
